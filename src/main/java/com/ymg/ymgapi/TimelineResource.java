package com.ymg.ymgapi;

import java.util.Iterator;
import java.util.List;

import javax.security.auth.login.LoginException;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.ITimelineDao;
import com.ymg.ymgapi.dao.TimelineDaoORMImpl;
import com.ymg.ymgapi.dto.AddTimelineReq;
import com.ymg.ymgapi.dto.AddTimelineRsp;
import com.ymg.ymgapi.dto.DeleteTimelineRsp;
import com.ymg.ymgapi.dto.ListTimelinesRsp;
import com.ymg.ymgapi.dto.UpdateTimelineReq;
import com.ymg.ymgapi.dto.UpdateTimelineRsp;
import com.ymg.ymgapi.inspector.Authenticate;
import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.util.Mapper;

@Path("/v1/{user}/timelines")
public class TimelineResource {
	private static final Logger log = LoggerFactory.getLogger(TimelineResource.class);
	
	private ITimelineDao timelineDao;
	
	public TimelineResource() {
		this.timelineDao = new TimelineDaoORMImpl();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ListTimelinesRsp listTimelines(@QueryParam("groupId") int groupId) {
		log.info("Enter TimelineResource.listTimelines(groupId={})", groupId);
		ListTimelinesRsp rsp = new ListTimelinesRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			List<com.ymg.ymgapi.dao.entity.Timeline> resultTimelines = this.timelineDao.findAll(groupId);
			
			if(resultTimelines != null) {
				for(Iterator<com.ymg.ymgapi.dao.entity.Timeline> timelineIter = resultTimelines.iterator(); timelineIter.hasNext();) {
					rsp.getTimelines().add(Mapper.map(timelineIter.next()));
				}
			}
			
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave TimelineResource.listTimelines");
		return rsp;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Authenticate
	public AddTimelineRsp addTimeline(AddTimelineReq req) {
		log.info("Enter TimelineResource.addTimeline");
		AddTimelineRsp rsp = new AddTimelineRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try{
			com.ymg.ymgapi.dao.entity.Timeline newTimeline = Mapper.map(req.getTimeline());
			
			int newTimelineId = this.timelineDao.insertTimeline(newTimeline);
			if(newTimelineId > 0) {
				rsp.setTimelineId(newTimelineId);
				
				rspStatus.setSuccess(true);
			} else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to add new timeline");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());			
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave TimelineResource.addTimeline(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Authenticate
	public UpdateTimelineRsp updateTimeline(@PathParam("user") String user, @PathParam("id") int id, UpdateTimelineReq req) {
		log.info("Enter TimelineResource.updateTimeline(user={})", user);
		UpdateTimelineRsp rsp = new UpdateTimelineRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			com.ymg.ymgapi.dao.entity.Timeline newTimeline = Mapper.map(req.getTimeline());
			if(this.timelineDao.updateTimeline(id, user, newTimeline))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to edit event");
			}
		} catch (LoginException e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add("Access denied");
		} catch (Exception e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(e.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave TimelineResource.updateTimeline(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Authenticate
	public DeleteTimelineRsp deleteTimeline(@PathParam("user") String user, @PathParam("id") int id) {
		log.info("Enter TimelineResource.deleteTimeline(user={})", user);
		DeleteTimelineRsp rsp = new DeleteTimelineRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			if(this.timelineDao.deleteTimeline(id, user))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to edit event");
			}
		} catch (LoginException e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add("Access denied");
		} catch (Exception e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(e.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave TimelineResource.deleteTimeline(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
}
