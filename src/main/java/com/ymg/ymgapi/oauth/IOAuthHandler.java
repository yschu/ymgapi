package com.ymg.ymgapi.oauth;

public interface IOAuthHandler {

	String getAuthUrl();
	
	OAuthAccessToken getAccessToken(String code);
	
	OAuthGraph getGraph(String accessToken);
	
}
