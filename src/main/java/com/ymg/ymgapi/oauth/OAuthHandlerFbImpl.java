package com.ymg.ymgapi.oauth;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class OAuthHandlerFbImpl implements IOAuthHandler {

	private static final Logger log = LoggerFactory.getLogger(OAuthHandlerFbImpl.class);
	
	private String appId;
	private String appSecret;
	private String redirectUrl;
	
	public OAuthHandlerFbImpl(String appId, String appSecret, String redirectUrl) {
		this.appId = appId;
		this.appSecret = appSecret;
		this.redirectUrl = redirectUrl;
	}
	
	public String getAuthUrl() {
		log.info("Enter OAuthHandlerFbImpl.getAuthUrl");
		
		String url = null;
		
		try {
			url = "http://www.facebook.com/dialog/oauth?client_id=" + this.appId + 
					"&redirect_uri=" + URLEncoder.encode(this.redirectUrl, "UTF-8") + 
					"&scope=public_profile";
		} catch (UnsupportedEncodingException e) {
			log.error("Unhandled exception", e);
		}
		
		log.info("Leave OAuthHandlerFbImpl.getAuthUrl");
		return url;
	}

	public OAuthAccessToken getAccessToken(String code) {
		log.info("Enter OAuthHandlerFbImpl.getAccessToken(code={})", code);
		
		OAuthAccessToken accessToken = null;
		
		try{
			String urlStr = "https://graph.facebook.com/oauth/access_token?client_id=" + this.appId +
								"&redirect_uri=" + URLEncoder.encode(this.redirectUrl, "UTF-8") + 
								"&client_secret=" + this.appSecret + 
								"&code=" + code;
			URL url = new URL(urlStr);
			URLConnection connection = url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	
			String line = null;
			StringBuffer sb = new StringBuffer();
			while((line = br.readLine()) != null)
				sb.append(line + "\r\n");
			
			ObjectMapper objMapper = new ObjectMapper();
			JsonNode rootNode = objMapper.readTree(sb.toString());
			
			accessToken = new OAuthAccessToken();
			accessToken.setAccessToken(rootNode.path("access_token").asText());
			accessToken.setExpiryInMilliSec(rootNode.path("expires_in").asLong());
			
		} catch(Exception e) {
			log.error("Unhandled exception", e);
		}
		
		log.info("Leave OAuthHandlerFbImpl.getAccessToken(accessToken={})", accessToken);
		return accessToken;
	}

	public OAuthGraph getGraph(String accessToken) {
		log.info("Enter OAuthHandlerFbImpl.getGraph");
		OAuthGraph graph = null;
		
		try{
			String urlStr = "https://graph.facebook.com/v2.8/me?fields=id,first_name,last_name,email";
			URL url = new URL(urlStr);
			HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			httpConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
			BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			
			String line = null;
			StringBuffer sb = new StringBuffer();
			while((line = br.readLine()) != null)
				sb.append(line + "\r\n");
			
			ObjectMapper objMapper = new ObjectMapper();
			JsonNode rootNode = objMapper.readTree(sb.toString());
			
			if(rootNode.path("error").isMissingNode()) {
				graph = new OAuthGraph();
				
				graph.setUserId(rootNode.path("id").asText());
				
				JsonNode emailNode = rootNode.path("email");
				if(!emailNode.isMissingNode())
					graph.setEmail(emailNode.asText());
				
				JsonNode firstNameNode = rootNode.findPath("first_name");
				if(!firstNameNode.isMissingNode())
					graph.setFirstName(firstNameNode.asText());
				
				JsonNode lastNameNode = rootNode.findPath("last_name");
				if(!lastNameNode.isMissingNode())
					graph.setLastName(lastNameNode.asText());
			}
			
		} catch(Exception e) {
			log.error("Unhandled exception", e);
		}
		
		log.info("Leave OAuthHandlerFbImpl.getGraph");
		return graph;
	}
	
}
