package com.ymg.ymgapi.oauth;

public class OAuthAccessToken {

	private String accessToken;
	private long expiryInMilliSec;
	
	public String getAccessToken() {
		return accessToken;
	}
	
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public long getExpiryInMilliSec() {
		return expiryInMilliSec;
	}

	public void setExpiryInMilliSec(long expiryInMilliSec) {
		this.expiryInMilliSec = expiryInMilliSec;
	}
	
	
}
