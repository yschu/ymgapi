package com.ymg.ymgapi;

import java.util.Iterator;
import java.util.List;

import javax.security.auth.login.LoginException;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.GroupDaoORMImpl;
import com.ymg.ymgapi.dao.IGroupDao;
import com.ymg.ymgapi.dto.AddGroupReq;
import com.ymg.ymgapi.dto.AddGroupRsp;
import com.ymg.ymgapi.dto.DeleteGroupRsp;
import com.ymg.ymgapi.dto.ListGroupsRsp;
import com.ymg.ymgapi.dto.UpdateGroupReq;
import com.ymg.ymgapi.dto.UpdateGroupRsp;
import com.ymg.ymgapi.inspector.Authenticate;
import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.util.Mapper;

@Path("/v1/{user}/groups")
public class GroupResource {

	private static final Logger log = LoggerFactory.getLogger(GroupResource.class);
	
	private IGroupDao groupDao;
	
	public GroupResource() {
		this.groupDao = new GroupDaoORMImpl();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ListGroupsRsp listGroups(@PathParam("user") String user) {
		log.info("Enter GroupResource.listGroups(user={})", user);
		ListGroupsRsp rsp = new ListGroupsRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			List<com.ymg.ymgapi.dao.entity.Group> resultGroups = this.groupDao.findAll(user);
			
			if(resultGroups != null) {
				for(Iterator<com.ymg.ymgapi.dao.entity.Group> groupIter = resultGroups.iterator(); groupIter.hasNext();) {
					rsp.getGroups().add(Mapper.map(groupIter.next()));
				}
			}
			
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave GroupResource.listGroups");
		return rsp;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Authenticate
	public AddGroupRsp addGroup(@PathParam("user") String user, AddGroupReq req) {
		log.info("Enter GroupResource.addGroup(user={})", user);
		AddGroupRsp rsp = new AddGroupRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try{
			com.ymg.ymgapi.dao.entity.Group newGroup = Mapper.map(req.getGroup());
			
			int newGroupId = this.groupDao.insertGroup(newGroup);
			if(newGroupId > 0) {
				rsp.setGroupId(newGroupId);
				
				rspStatus.setSuccess(true);
			} else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to add new group");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());			
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave GroupResource.addGroup(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Authenticate
	public UpdateGroupRsp updateGroup(@PathParam("user") String user, @PathParam("id") int id, UpdateGroupReq req) {
		log.info("Enter GroupResource.updateGroup(user={})", user);
		UpdateGroupRsp rsp = new UpdateGroupRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			com.ymg.ymgapi.dao.entity.Group newGroup = Mapper.map(req.getGroup());
			if(this.groupDao.updateGroup(id, user, newGroup))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to edit event");
			}
		} catch (LoginException e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add("Access denied");
		} catch (Exception e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(e.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave GroupResource.updateGroup(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Authenticate
	public DeleteGroupRsp deleteGroup(@PathParam("user") String user, @PathParam("id") int id) {
		log.info("Enter GroupResource.deleteGroup(user={})", user);
		DeleteGroupRsp rsp = new DeleteGroupRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			if(this.groupDao.deleteGroup(id, user))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to edit event");
			}
		} catch (LoginException e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add("Access denied");
		} catch (Exception e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(e.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave GroupResource.deleteGroup(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
}
