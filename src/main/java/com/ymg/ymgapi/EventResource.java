package com.ymg.ymgapi;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.login.LoginException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.EventDaoORMImpl;
import com.ymg.ymgapi.dao.IEventDao;
import com.ymg.ymgapi.dto.*;
import com.ymg.ymgapi.inspector.Authenticate;
import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.util.Mapper;

@Path("/v1/{user}/events")
public class EventResource {

	private static final Logger log = LoggerFactory.getLogger(EventResource.class);
	
	private IEventDao eventDao;
	
	public EventResource(){
		this.eventDao = new EventDaoORMImpl();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ListEventsRsp listEvents(@PathParam("user") String user, 
			@QueryParam("orderby") String orderByParam, @QueryParam("starttime") String startTimeParam, 
			@QueryParam("limit") String limit, @QueryParam("endtime") String endTime) {
		log.info("Enter EventResource.listEvents(user={})", user);		
		ListEventsRsp rsp = new ListEventsRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			String startTime = startTimeParam;
			if(startTime == null)
				startTime = Long.toString((new Date()).getTime());
			
			String orderBy = orderByParam;
			if(orderBy == null)
				orderBy = "ASC";
			
			if(limit == null && endTime == null) {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Both limit & end-time are null");
			} else {
				List<com.ymg.ymgapi.dao.entity.Event> resultEvents = null;
				
				Date startDateTime = new Date(Long.parseLong(startTime));
				
				if(endTime != null) {
					Date endDateTime = new Date(Long.parseLong(endTime));
					resultEvents = getEventsBetween(user, startDateTime, endDateTime, orderBy);
				} else {
					resultEvents = getEventsByLimit(user, startDateTime, Integer.parseInt(limit), orderBy);
				}
				
				if(resultEvents != null) {
					for(Iterator<com.ymg.ymgapi.dao.entity.Event> eventIter = resultEvents.iterator(); eventIter.hasNext();){
						rsp.getEvents().add(Mapper.map(eventIter.next()));
					}
				}
				
				rspStatus.setSuccess(true);
			}
			
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave EventResource.listEvents");
		return rsp;
	}
	
	private List<com.ymg.ymgapi.dao.entity.Event> getEventsBetween(String user, Date startDateTime, Date endDateTime, String orderBy) throws Exception {
		return this.eventDao.findBetween(user, startDateTime, endDateTime, orderBy);
	}
	
	private List<com.ymg.ymgapi.dao.entity.Event> getEventsByLimit(String user, Date startDateTime, int limit, String orderBy) throws Exception {
		return this.eventDao.findByLimit(user, startDateTime, limit, orderBy);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Authenticate
	public AddEventRsp addEvent(@PathParam("user") String user, AddEventReq req) {
		log.info("Enter EventResource.addEvent(user={})", user);
		AddEventRsp rsp = new AddEventRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			com.ymg.ymgapi.dao.entity.Event newEvent = Mapper.map(req.getEvent());
			
			int newEventId = this.eventDao.insertEvent(newEvent); 
			if(newEventId > 0) {
				rsp.setEventId(newEventId);
				
				rspStatus.setSuccess(true);
			} else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to add new event");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave EventResource.addEvent(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Authenticate
	public UpdateEventRsp updateEvent(@PathParam("user") String user, @PathParam("id") int id, UpdateEventReq req) {
		log.info("Enter EventResource.updateEvent(user={})", user);
		UpdateEventRsp rsp = new UpdateEventRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			com.ymg.ymgapi.dao.entity.Event newEvent = Mapper.map(req.getEvent());
			if(this.eventDao.updateEvent(id, user, newEvent))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to edit event");
			}
		} catch (LoginException e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add("Access denied");
		} catch (Exception e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(e.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave EventResource.updateEvent(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Authenticate
	public DeleteEventRsp deleteEvent(@PathParam("user") String user, @PathParam("id") int id) {
		log.info("Enter EventResource.deleteEvent(user={})", user);
		DeleteEventRsp rsp = new DeleteEventRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			if(this.eventDao.deleteEvent(id, user))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to edit event");
			}
		} catch (LoginException e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add("Access denied");
		} catch (Exception e) {
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(e.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave EventResource.deleteEvent(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
}
