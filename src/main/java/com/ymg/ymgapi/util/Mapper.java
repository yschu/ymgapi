package com.ymg.ymgapi.util;

import java.util.Iterator;

public class Mapper {
	
	public static com.ymg.ymgapi.model.Event map(com.ymg.ymgapi.dao.entity.Event src) {
		com.ymg.ymgapi.model.Event dest = new com.ymg.ymgapi.model.Event();
		dest.setId(src.getId());
		dest.setOwner(src.getOwner());
		dest.setName(src.getName());
		dest.setDescription(src.getDescription());
		dest.setUrl(src.getUrl());
		for(Iterator<com.ymg.ymgapi.dao.entity.EventImage> evtImgIter = src.getImages().iterator(); evtImgIter.hasNext();) {
			dest.getImages().add(Mapper.map(evtImgIter.next()));
		}
		for(Iterator<com.ymg.ymgapi.dao.entity.EventSession> evtSessionIter = src.getSessions().iterator(); evtSessionIter.hasNext();) {
			dest.getSessions().add(Mapper.map(evtSessionIter.next()));
		}
		for(Iterator<com.ymg.ymgapi.dao.entity.EventTag> evtTagIter = src.getTags().iterator(); evtTagIter.hasNext();) {
			dest.getTags().add(Mapper.map(evtTagIter.next()));
		}
		for(Iterator<com.ymg.ymgapi.dao.entity.User> userIter = src.getAttendees().iterator(); userIter.hasNext();) {
			dest.getAttendees().add(Mapper.map(userIter.next()));
		}
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.Event map(com.ymg.ymgapi.model.Event src) {
		com.ymg.ymgapi.dao.entity.Event dest = new com.ymg.ymgapi.dao.entity.Event();
		// Should not map Id, as it should be auto generated
		// dest.setId(src.getId());
		dest.setOwner(src.getOwner());
		dest.setName(src.getName());
		dest.setDescription(src.getDescription());
		dest.setUrl(src.getUrl());
		for(Iterator<com.ymg.ymgapi.model.EventImage> evtImgIter = src.getImages().iterator(); evtImgIter.hasNext();) {
			com.ymg.ymgapi.dao.entity.EventImage destEvtImg = Mapper.map(evtImgIter.next());
			destEvtImg.setEvent(dest);
			dest.getImages().add(destEvtImg);
		}
		for(Iterator<com.ymg.ymgapi.model.EventSession> evtSessionIter = src.getSessions().iterator(); evtSessionIter.hasNext();) {
			com.ymg.ymgapi.dao.entity.EventSession destEvtSession = Mapper.map(evtSessionIter.next());
			destEvtSession.setEvent(dest);
			dest.getSessions().add(destEvtSession);
		}
		for(Iterator<com.ymg.ymgapi.model.EventTag> evtTagIter = src.getTags().iterator(); evtTagIter.hasNext();) {
			com.ymg.ymgapi.dao.entity.EventTag destEvtTag = Mapper.map(evtTagIter.next());
			destEvtTag.setEvent(dest);
			dest.getTags().add(destEvtTag);
		}
		for(Iterator<com.ymg.ymgapi.model.User> userIter = src.getAttendees().iterator(); userIter.hasNext();) {
			com.ymg.ymgapi.dao.entity.User destUser = Mapper.map(userIter.next());
			destUser.getEvents().add(dest);
			dest.getAttendees().add(destUser);
		}
		return dest;
	}

	public static com.ymg.ymgapi.model.EventImage map(com.ymg.ymgapi.dao.entity.EventImage src) {
		com.ymg.ymgapi.model.EventImage dest = new com.ymg.ymgapi.model.EventImage();
		dest.setUrl(src.getUrl());
		dest.setDefault(src.isDefault());
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.EventImage map(com.ymg.ymgapi.model.EventImage src) {
		com.ymg.ymgapi.dao.entity.EventImage dest = new com.ymg.ymgapi.dao.entity.EventImage();
		dest.setUrl(src.getUrl());
		dest.setDefault(src.isDefault());
		return dest;
	}
	
	public static com.ymg.ymgapi.model.EventSession map(com.ymg.ymgapi.dao.entity.EventSession src) {
		com.ymg.ymgapi.model.EventSession dest = new com.ymg.ymgapi.model.EventSession();		
		dest.setFrom(src.getFrom());
		dest.setTo(src.getTo());
		dest.setVenue(src.getVenue());
		dest.setDescription(src.getDescription());
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.EventSession map(com.ymg.ymgapi.model.EventSession src) {
		com.ymg.ymgapi.dao.entity.EventSession dest = new com.ymg.ymgapi.dao.entity.EventSession();
		dest.setFrom(src.getFrom());
		dest.setTo(src.getTo());
		dest.setVenue(src.getVenue());
		dest.setDescription(src.getDescription());
		return dest;
	}
	
	public static com.ymg.ymgapi.model.EventTag map(com.ymg.ymgapi.dao.entity.EventTag src) {
		com.ymg.ymgapi.model.EventTag dest = new com.ymg.ymgapi.model.EventTag();
		dest.setTag(src.getTag());
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.EventTag map(com.ymg.ymgapi.model.EventTag src) {
		com.ymg.ymgapi.dao.entity.EventTag dest = new com.ymg.ymgapi.dao.entity.EventTag();
		// Should not map Id, as it should be auto generated
		// dest.setId(src.getId());
		dest.setTag(src.getTag());
		return dest;
	}
	
	public static com.ymg.ymgapi.model.User map(com.ymg.ymgapi.dao.entity.User src) {
		com.ymg.ymgapi.model.User dest = new com.ymg.ymgapi.model.User();
		dest.setId(src.getId());
		dest.setUsername(src.getUsername());
		dest.setDisplayName(src.getDisplayName());
		dest.setEmail(src.getEmail());
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.User map(com.ymg.ymgapi.model.User src) {
		com.ymg.ymgapi.dao.entity.User dest = new com.ymg.ymgapi.dao.entity.User();
		dest.setId(src.getId());
		dest.setUsername(src.getUsername());
		dest.setEmail(src.getEmail());
		dest.setDisplayName(src.getDisplayName());
		return dest;
	}
	
	public static com.ymg.ymgapi.model.OAuthGraph map(com.ymg.ymgapi.oauth.OAuthGraph src) {
		com.ymg.ymgapi.model.OAuthGraph dest = new com.ymg.ymgapi.model.OAuthGraph();
		dest.setUserId(src.getUserId());
		dest.setFirstName(src.getFirstName());
		dest.setLastName(src.getLastName());
		dest.setEmail(src.getEmail());
		return dest;
	}
	
	public static com.ymg.ymgapi.model.Group map(com.ymg.ymgapi.dao.entity.Group src) {
		com.ymg.ymgapi.model.Group dest = new com.ymg.ymgapi.model.Group();
		dest.setId(src.getId());
		dest.setName(src.getName());
		for(Iterator<com.ymg.ymgapi.dao.entity.User> userIter = src.getMembers().iterator(); userIter.hasNext();) {
			dest.getMembers().add(Mapper.map(userIter.next()));
		}
		for(Iterator<com.ymg.ymgapi.dao.entity.Event> eventIter = src.getEvents().iterator(); eventIter.hasNext();) {
			dest.getEvents().add(Mapper.map(eventIter.next()));
		}
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.Group map(com.ymg.ymgapi.model.Group src) {
		com.ymg.ymgapi.dao.entity.Group dest = new com.ymg.ymgapi.dao.entity.Group();
		// Should not map Id, as it should be auto generated
		// dest.setId(src.getId());
		dest.setName(src.getName());
		for(Iterator<com.ymg.ymgapi.model.User> userIter = src.getMembers().iterator(); userIter.hasNext();) {
			dest.getMembers().add(Mapper.map(userIter.next()));
		}
		for(Iterator<com.ymg.ymgapi.model.Event> eventIter = src.getEvents().iterator();eventIter.hasNext();) {
			dest.getEvents().add(Mapper.map(eventIter.next()));
		}
		return dest;
	}
	
	public static com.ymg.ymgapi.model.Timeline map(com.ymg.ymgapi.dao.entity.Timeline src) {
		com.ymg.ymgapi.model.Timeline dest = new com.ymg.ymgapi.model.Timeline();
		dest.setId(src.getId());
		dest.setGroup(Mapper.map(src.getGroup()));
		for(Iterator<com.ymg.ymgapi.dao.entity.Event> eventIter = src.getEvents().iterator(); eventIter.hasNext();) {
			dest.getEvents().add(Mapper.map(eventIter.next()));
		}
		return dest;
	}
	
	public static com.ymg.ymgapi.dao.entity.Timeline map(com.ymg.ymgapi.model.Timeline src) {
		com.ymg.ymgapi.dao.entity.Timeline dest = new com.ymg.ymgapi.dao.entity.Timeline();
		// Should not map Id, as it should be auto generated
		// dest.setId(src.getId());
		dest.setGroup(Mapper.map(src.getGroup()));
		for(Iterator<com.ymg.ymgapi.model.Event> eventIter = src.getEvents().iterator();eventIter.hasNext();) {
			dest.getEvents().add(Mapper.map(eventIter.next()));
		}
		return dest;
	}
}
