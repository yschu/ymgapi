package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class AddGroupRsp {

	private ResponseStatus rspStatus; 
	private int groupId;
	
	public int getGroupId() {
		return groupId;
	}
	
	@XmlElement
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
	
}
