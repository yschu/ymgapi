package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.OAuthGraph;
import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class GetOAuthGraphRsp {

	private ResponseStatus rspStatus;
	private OAuthGraph graph;

	public OAuthGraph getGraph() {
		return graph;
	}

	@XmlElement
	public void setGraph(OAuthGraph graph) {
		this.graph = graph;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}

	
}
