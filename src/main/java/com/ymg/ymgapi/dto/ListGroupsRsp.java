package com.ymg.ymgapi.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.Group;
import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class ListGroupsRsp {

	private ResponseStatus rspStatus;
	private List<Group> groups;
	
	public ListGroupsRsp() {
		this.setGroups(new ArrayList<Group>());
	}
	
	public ResponseStatus getRspStatus() {
		return rspStatus;
	}
	
	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
	
	public List<Group> getGroups() {
		return groups;
	}
	
	@XmlElement
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	
}
