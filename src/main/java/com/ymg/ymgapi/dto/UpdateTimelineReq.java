package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.Timeline;

@XmlRootElement
public class UpdateTimelineReq {

	private Timeline timeline;

	public Timeline getTimeline() {
		return timeline;
	}

	@XmlElement
	public void setTimeline(Timeline timeline) {
		this.timeline = timeline;
	}
}
