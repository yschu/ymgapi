package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class GetAuthUrlRsp {

	private ResponseStatus rspStatus;
	private String url;

	public String getUrl() {
		return url;
	}

	@XmlElement
	public void setUrl(String url) {
		this.url = url;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}

	
	
}
