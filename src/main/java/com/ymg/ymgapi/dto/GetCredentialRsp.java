package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class GetCredentialRsp {

	private ResponseStatus rspStatus;
	private boolean isExist;

	public boolean isExist() {
		return isExist;
	}

	@XmlElement
	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
	
}
