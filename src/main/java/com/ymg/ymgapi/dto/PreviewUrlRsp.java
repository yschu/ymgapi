package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.Event;
import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class PreviewUrlRsp {

	private ResponseStatus rspStatus;
	private Event event;

	public Event getEvent() {
		return event;
	}

	@XmlElement
	public void setEvent(Event event) {
		this.event = event;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}

}
