package com.ymg.ymgapi.dto;

import java.util.ArrayList;
import java.util.List;

import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.model.Timeline;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListTimelinesRsp {

	private ResponseStatus rspStatus;
	private List<Timeline> timelines;
	
	public ListTimelinesRsp() {
		this.setTimelines(new ArrayList<Timeline>());
	}
	
	public ResponseStatus getRspStatus() {
		return rspStatus;
	}
	
	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
	
	public List<Timeline> getTimelines() {
		return timelines;
	}
	
	@XmlElement
	public void setTimelines(List<Timeline> timelines) {
		this.timelines = timelines;
	}
	
}
