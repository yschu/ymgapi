package com.ymg.ymgapi.dto;

import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.model.User;

@XmlRootElement
public class OAuthSignInRsp {

	private ResponseStatus rspStatus;
	private User user;
	private UUID accessToken;

	public UUID getAccessToken() {
		return accessToken;
	}

	@XmlElement
	public void setAccessToken(UUID accessToken) {
		this.accessToken = accessToken;
	}

	public User getUser() {
		return user;
	}

	@XmlElement
	public void setUser(User user) {
		this.user = user;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
}
