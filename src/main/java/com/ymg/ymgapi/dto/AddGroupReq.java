package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.Group;

@XmlRootElement
public class AddGroupReq {
	
	private Group group;

	public Group getGroup() {
		return group;
	}

	@XmlElement
	public void setGroup(Group group) {
		this.group = group;
	}
	
}
