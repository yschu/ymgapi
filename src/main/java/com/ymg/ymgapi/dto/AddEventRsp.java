package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class AddEventRsp {
	
	private ResponseStatus rspStatus; 
	private int eventId;

	public int getEventId() {
		return eventId;
	}

	@XmlElement
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
}
