package com.ymg.ymgapi.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.Event;
import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class ListEventsRsp {

	private ResponseStatus rspStatus;
	private List<Event> events;
	
	public ListEventsRsp(){
		this.setEvents(new ArrayList<Event>());
	}

	public List<Event> getEvents() {
		return events;
	}

	@XmlElement
	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}
	
}
