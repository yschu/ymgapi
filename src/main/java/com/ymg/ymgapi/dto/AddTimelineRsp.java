package com.ymg.ymgapi.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ymg.ymgapi.model.ResponseStatus;

@XmlRootElement
public class AddTimelineRsp {

	private ResponseStatus rspStatus; 
	private int timelineId;

	public ResponseStatus getRspStatus() {
		return rspStatus;
	}

	@XmlElement
	public void setRspStatus(ResponseStatus rspStatus) {
		this.rspStatus = rspStatus;
	}

	public int getTimelineId() {
		return timelineId;
	}

	@XmlElement
	public void setTimelineId(int timelineId) {
		this.timelineId = timelineId;
	}
}
