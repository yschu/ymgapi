package com.ymg.ymgapi.dao;

import java.util.List;

import javax.security.auth.login.LoginException;

import com.ymg.ymgapi.dao.entity.Group;

public interface IGroupDao {

	public List<Group> findAll(String user) throws Exception;
	
	public int insertGroup(Group group) throws Exception;
	
	public boolean updateGroup(int groupId, String user, Group newGroup) throws LoginException, Exception;
	
	public boolean deleteGroup(int groupId, String user) throws LoginException, Exception;
}
