package com.ymg.ymgapi.dao;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.entity.Event;
import com.ymg.ymgapi.dao.entity.EventSession;

public class EventDaoORMImpl implements IEventDao {

	private static final Logger log = LoggerFactory.getLogger(EventDaoORMImpl.class);
	
	private static EntityManagerFactory emf;
	
	public EventDaoORMImpl(){
		emf = Persistence.createEntityManagerFactory("ymg-unit");
	}
	
	public List<Event> findAll(String user) throws Exception {
		log.info("Enter EventDaoORMImpl.findAll");
		List<Event> events = null;
		
        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();

            events = em.createQuery("SELECT e FROM Event e WHERE owner = :owner ORDER BY e.id DESC", Event.class)
            			.setParameter("owner", user)
            			.getResultList();

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
        log.info("Leave EventDaoORMImpl.findAll");
        return events;
	}
	
	public List<Event> findBetween(String user, Date startDateTime, Date endDateTime, String orderBy) throws Exception {
		log.info("Enter EventDaoORMImpl.findBetween");
		List<Event> events = null;
		
        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();

            String query = "SELECT e FROM Event AS e, EventSession AS s WHERE s MEMBER OF e.sessions AND s.from BETWEEN :startTime AND :endTime AND e.owner = :owner ORDER BY s.from ASC, e.id ASC";
            if("desc".equals(orderBy.toLowerCase()))
            	query = "SELECT e FROM Event AS e, EventSession AS s WHERE s MEMBER OF e.sessions AND s.from BETWEEN :startTime AND :endTime AND e.owner = :owner ORDER BY s.from DESC, e.id DESC";
            
            events = em.createQuery(query, Event.class)
            			.setParameter("owner", user)
            			.setParameter("startTime", startDateTime)
            			.setParameter("endTime", endDateTime)
            			.getResultList();

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
        log.info("Leave EventDaoORMImpl.findBetween");
        return events;
	}
	
	public List<Event> findByLimit(String user, Date startDateTime, int limit, String orderBy) throws Exception {
		log.info("Enter EventDaoORMImpl.findByLimit");
		List<Event> events = null;
		
        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();

            String query = "SELECT e FROM Event AS e, EventSession AS s WHERE s MEMBER OF e.sessions AND s.from >= :startTime AND e.owner = :owner ORDER BY s.from ASC, e.id ASC";
            if("desc".equals(orderBy.toLowerCase()))
            	query = "SELECT e FROM Event AS e, EventSession AS s WHERE s MEMBER OF e.sessions AND s.from <= :startTime AND e.owner = :owner ORDER BY s.from DESC, e.id DESC";
            
            events = em.createQuery(query, Event.class)
            			.setParameter("owner", user)
            			.setParameter("startTime", startDateTime)
            			.setMaxResults(limit)
            			.getResultList();

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
        log.info("Leave EventDaoORMImpl.findByLimit");
        return events;
	}

	public int insertEvent(Event event) throws Exception {
		log.info("Enter EventDaoORMImpl.insertEvent");
		int eventId = 0;
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction trx = null;
		
		try{
			trx = em.getTransaction();
			trx.begin();
			
			em.persist(event);
			
			trx.commit();
			eventId = event.getId();
		} catch(Exception ex){
			if(trx != null)
				trx.rollback();
			
			log.error("Unhandled exception", ex);
			throw ex;
		} finally{
			em.close();
		}
		
		log.info("Leave EventDaoORMImpl.insertEvent");
		return eventId;
	}

	
	public boolean updateEvent(int eventId, String user, Event newEvent) throws LoginException, Exception {
		log.info("Enter EventDaoORMImpl.updateEvent(eventId={})", eventId);
		boolean result = false;
		
		EntityManager em = emf.createEntityManager();

		// Get the Event object
        Event event = em.find(Event.class, eventId);
        if(!event.getOwner().equals(user))
        	throw new LoginException("Event: " + eventId + " is not belong to " + user);
		
        EntityTransaction trx = null;
        try {
            // Get a transaction
            trx = em.getTransaction();
            // Begin the transaction
            trx.begin();          
                        	
            // Change the values
            if(newEvent.getName() != null)
            	event.setName(newEvent.getName());
            
            if(newEvent.getDescription() != null)
            	event.setDescription(newEvent.getDescription());
            
            if(newEvent.getUrl() != null)
            	event.setUrl(newEvent.getUrl());
            
            if(newEvent.getSessions() != null && !newEvent.getSessions().isEmpty()) {
	            event.getSessions().clear();
	            for(Iterator<EventSession> evtSessionItr = newEvent.getSessions().iterator(); evtSessionItr.hasNext();) {
	            	EventSession newEventSession = evtSessionItr.next();
	            	newEventSession.setEvent(event);
	            	em.persist(newEventSession);
	            	event.getSessions().add(newEventSession);
	            }
            }
            
            if(newEvent.getAttendees() != null && !newEvent.getAttendees().isEmpty()) {
            	//for(Iterator<User> attendeesItr = newEvent.getAttendees().iterator(); attendeesItr.hasNext();) {
            	//	int attendeeUserId = attendeesItr.next().getId();
            	//	User attendee = em.find(User.class, attendeeUserId);
            	//	if(!isAttended(event, attendee)) {
            	//		attendee.getEvents().add(event);
            	//		event.getAttendees().add(attendee);
            	//	}
            	//}
            	event.setAttendees(newEvent.getAttendees());
            }
            
            // Commit the transaction
            trx.commit();
            result = true;            
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (trx != null) 
                trx.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
        
        log.info("Leave EventDaoORMImpl.updateEvent");
        return result;
	}

	/*
	private boolean isAttended(Event event, User attendee) {
		for(Iterator<User> attendeeItr = event.getAttendees().iterator(); attendeeItr.hasNext();) {
			if(attendeeItr.next().getId() == attendee.getId())
				return true;
		}
		
		for(Iterator<Event> eventItr = attendee.getEvents().iterator(); eventItr.hasNext();) {
			if(eventItr.next().getId() == event.getId())
				return true;
		}
		
		return false;
	}
	*/
	
	
	public boolean deleteEvent(int eventId, String user) throws LoginException, Exception {
		log.info("Enter EventDaoORMImpl.deleteEvent(eventId={})", eventId);
		boolean result = false;
		
		EntityManager em = emf.createEntityManager();

		// Get the Event object
        Event event = em.find(Event.class, eventId);
        if(!event.getOwner().equals(user))
        	throw new LoginException("Event: " + eventId + " is not belong to " + user);
		
        EntityTransaction trx = null;
        try {
            // Get a transaction
            trx = em.getTransaction();
            // Begin the transaction
            trx.begin();          
                        	
            em.remove(event);

            // Commit the transaction
            trx.commit();
            result = true;            
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (trx != null) 
                trx.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
        
        log.info("Leave EventDaoORMImpl.deleteEvent");
        return result;
	}


}
