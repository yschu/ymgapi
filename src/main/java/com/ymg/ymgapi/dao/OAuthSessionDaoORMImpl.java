package com.ymg.ymgapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.entity.OAuthSession;

public class OAuthSessionDaoORMImpl implements IOAuthSessionDao{
	
	private static final Logger log = LoggerFactory.getLogger(OAuthSessionDaoORMImpl.class);
	
	private static EntityManagerFactory emf;
	
	public OAuthSessionDaoORMImpl(){
		emf = Persistence.createEntityManagerFactory("ymg-unit");
	}
	public boolean addOAuthSession(OAuthSession oAuthSession) {
		log.info("Enter OAuthSessionDaoORMImpl.addOAuthSession");
		boolean result = false;
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction trx = null;
		
		try{
			trx = em.getTransaction();
			trx.begin();
			
			em.persist(oAuthSession);
			
			trx.commit();
			result = true;
		} catch(Exception ex) {
			if(trx != null)
				trx.rollback();
			
			log.error("Unhandled exception", ex);
		} finally {
			em.close();
		}
		
		log.info("Leave OAuthSessionDaoORMImpl.addOAuthSession");
		return result;
	}

	public OAuthSession getOAuthSession(String username) {
		log.info("Enter OAuthSessionDaoORMImpl.getOAuthSession");
		OAuthSession session = null;

        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Get a List of Sessions
            List<OAuthSession> sessions = em.createQuery("SELECT s FROM OAuthSession s WHERE s.username = :username ORDER BY s.createDateTime DESC", OAuthSession.class)
            		.setParameter("username", username)
            		.setMaxResults(1)
            		.getResultList();
            
            if(sessions.size() > 0)
            	session = sessions.get(0);
            
            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();
            
            log.error("Unhandled exception", ex);
        } finally {
            // Close the EntityManager
            em.close();
        }
        
        log.info("Leave OAuthSessionDaoORMImpl.getOAuthSession");
        return session;
	}

}
