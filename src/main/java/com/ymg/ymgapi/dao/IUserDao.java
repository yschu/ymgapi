package com.ymg.ymgapi.dao;

import java.util.List;

import com.ymg.ymgapi.dao.entity.User;

public interface IUserDao {

	public List<User> getAllUsers();
	
	public User getUser(int userId);
	
	public boolean addUser(User user);
	
}
