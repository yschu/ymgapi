package com.ymg.ymgapi.dao;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.entity.User;

public class UserDaoORMImpl implements IUserDao {

	private static final Logger log = LoggerFactory.getLogger(UserDaoORMImpl.class);
	
	private static EntityManagerFactory emf;
	
	public UserDaoORMImpl(){
		emf = Persistence.createEntityManagerFactory("ymg-unit");
	}
	
	public List<User> getAllUsers() {
		log.info("Enter UserDaoORMImpl.getAllUsers");
		List<User> users = null;

        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Get a List of Students
            users = em.createQuery("SELECT u FROM User u", User.class).getResultList();

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
        } finally {
            // Close the EntityManager
            em.close();
        }
        
        log.info("Leave UserDaoORMImpl.getAllUsers");
        return users;
	}

	public User getUser(int userId) {
		log.info("Enter UserDaoORMImpl.getUser");
		User result = null;
		
		List<User> users = this.getAllUsers();
		for(Iterator<User> userIter = users.iterator(); userIter.hasNext();){
			User tmpUser = userIter.next();
			if(tmpUser.getId() == userId)
			{
				result = tmpUser;
				break;
			}
		}
		
		log.info("Leave UserDaoORMImpl.getUser");
		return result;
	}

	public boolean addUser(User user) {
		log.info("Enter UserDaoORMImpl.addUser");		
		boolean result = false;
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction trx = null;
		
		try{
			trx = em.getTransaction();
			trx.begin();
			
			em.persist(user);
			
			trx.commit();
			result = true;
		} catch(Exception ex){
			if(trx != null)
				trx.rollback();
			
			log.error("Unhandled exception", ex);
		} finally {
			em.close();
		}
		log.info("Leave UserDaoORMImpl.addUser");
		return result;
	}

}
