package com.ymg.ymgapi.dao;

import com.ymg.ymgapi.dao.entity.OAuthSession;

public interface IOAuthSessionDao {

	public boolean addOAuthSession(OAuthSession oAuthSession);
	
	public OAuthSession getOAuthSession(String username);
	
}
