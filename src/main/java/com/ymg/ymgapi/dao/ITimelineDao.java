package com.ymg.ymgapi.dao;

import java.util.List;

import javax.security.auth.login.LoginException;

import com.ymg.ymgapi.dao.entity.Timeline;

public interface ITimelineDao {

	public List<Timeline> findAll(int groupId) throws Exception;
	
	public int insertTimeline(Timeline timeline) throws Exception;
	
	public boolean updateTimeline(int timelineId, String user, Timeline newTimeline) throws LoginException, Exception;
	
	public boolean deleteTimeline(int timelineId, String user) throws LoginException, Exception;
}
