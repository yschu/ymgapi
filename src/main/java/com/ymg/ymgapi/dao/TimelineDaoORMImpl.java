package com.ymg.ymgapi.dao;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.entity.Timeline;
import com.ymg.ymgapi.dao.entity.User;

public class TimelineDaoORMImpl implements ITimelineDao {

private static final Logger log = LoggerFactory.getLogger(TimelineDaoORMImpl.class);
	
	private static EntityManagerFactory emf;
	
	public TimelineDaoORMImpl(){
		emf = Persistence.createEntityManagerFactory("ymg-unit");
	}
	
	public List<Timeline> findAll(int groupId) throws Exception {
		log.info("Enter TimelineDaoORMImpl.findAll");
		List<Timeline> timelines = null;
		
        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            String query = "SELECT g FROM Timeline AS t, Group AS g WHERE t MEMBER OF g.timelines AND g.id = :groupId ORDER BY t.id ASC";
            
            timelines = em.createQuery(query, Timeline.class)
            			.setParameter("groupId", groupId)
            			.getResultList();
            
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave TimelineDaoORMImpl.findAll");
		return timelines;
	}

	public int insertTimeline(Timeline timeline) throws Exception {
		log.info("Enter TimelineDaoORMImpl.insertTimeline");
		int timelineId = 0;
		
		// Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            em.persist(timeline);
            
            transaction.commit();
            timelineId = timeline.getId();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave TimelineDaoORMImpl.insertTimeline");
		return timelineId;
	}

	public boolean updateTimeline(int timelineId, String user, Timeline newTimeline) throws LoginException, Exception {
		log.info("Enter TimelineDaoORMImpl.updateTimeline(timelineId={})", timelineId);
		boolean result = false;
		
		// Create an EntityManager
        EntityManager em = emf.createEntityManager();
        
        Timeline timeline = em.find(Timeline.class, timelineId);
        if(!isMember(timeline, user))
        	throw new LoginException("User: " + user + " cannot access timeline: " + timelineId);
        
        EntityTransaction transaction = null;
        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            if(newTimeline.getEvents() != null && !newTimeline.getEvents().isEmpty())
            	timeline.setEvents(newTimeline.getEvents());
            
            transaction.commit();
            result = true;
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave TimelineDaoORMImpl.updateTimeline");
        return result;
	}

	private boolean isMember(Timeline timeline, String username) {
		boolean result = false;
		
		for(Iterator<User> memberIter = timeline.getGroup().getMembers().iterator(); memberIter.hasNext();) {
			if(memberIter.next().getUsername() == username) {
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	public boolean deleteTimeline(int timelineId, String user) throws LoginException, Exception {
		log.info("Enter TimelineDaoORMImpl.deleteTimeline(timelineId={})", timelineId);
		boolean result = false;
		
		// Create an EntityManager
        EntityManager em = emf.createEntityManager();
        
        Timeline timeline = em.find(Timeline.class, timelineId);
        if(!isMember(timeline, user))
        	throw new LoginException("User: " + user + " cannot access timeline: " + timelineId);
        
        EntityTransaction transaction = null;
        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            em.remove(timeline);
            
            transaction.commit();
            result = true;
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave TimelineDaoORMImpl.deleteTimeline");
        return result;
	}
}
