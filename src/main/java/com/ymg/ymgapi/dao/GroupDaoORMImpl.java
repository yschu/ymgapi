package com.ymg.ymgapi.dao;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.entity.Group;
import com.ymg.ymgapi.dao.entity.User;

public class GroupDaoORMImpl implements IGroupDao{

	private static final Logger log = LoggerFactory.getLogger(GroupDaoORMImpl.class);
	
	private static EntityManagerFactory emf;
	
	public GroupDaoORMImpl(){
		emf = Persistence.createEntityManagerFactory("ymg-unit");
	}
	
	public List<Group> findAll(String user) throws Exception {
		log.info("Enter GroupDaoORMImpl.findAll");
		List<Group> groups = null;
		
        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            String query = "SELECT g FROM Group AS g, USER AS u WHERE u MEMBER OF g.members AND u.username = :user ORDER BY g.id ASC";
            
            groups = em.createQuery(query, Group.class)
            			.setParameter("user", user)
            			.getResultList();
            
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave GroupDaoORMImpl.findAll");
		return groups;
	}

	public int insertGroup(Group group) throws Exception {
		log.info("Enter GroupDaoORMImpl.insertGroup");
		int groupId = 0;
		
		// Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            em.persist(group);
            
            transaction.commit();
            groupId = group.getId();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave GroupDaoORMImpl.insertGroup");
		return groupId;
	}

	public boolean updateGroup(int groupId, String user, Group newGroup) throws LoginException, Exception {
		log.info("Enter GroupDaoORMImpl.updateGroup(groupId={})", groupId);
		boolean result = false;
		
		// Create an EntityManager
        EntityManager em = emf.createEntityManager();
        
        Group group = em.find(Group.class, groupId);
        if(!isMember(group, user))
        	throw new LoginException("User: " + user + " is not member of Group: " + groupId);
        
        EntityTransaction transaction = null;
        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            if(newGroup.getName() != null)
            	group.setName(newGroup.getName());
            
            if(newGroup.getMembers() != null && !newGroup.getMembers().isEmpty())
            	group.setMembers(newGroup.getMembers());
            
            if(newGroup.getEvents() != null && !newGroup.getEvents().isEmpty())
            	group.setEvents(newGroup.getEvents());
            
            transaction.commit();
            result = true;
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave GroupDaoORMImpl.updateGroup");
        return result;
	}

	private boolean isMember(Group group, String username) {
		boolean result = false;
		
		for(Iterator<User> memberIter = group.getMembers().iterator(); memberIter.hasNext();) {
			if(memberIter.next().getUsername() == username) {
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	public boolean deleteGroup(int groupId, String user) throws LoginException, Exception {
		log.info("Enter GroupDaoORMImpl.deleteGroup(groupId={})", groupId);
		boolean result = false;
		
		// Create an EntityManager
        EntityManager em = emf.createEntityManager();
        
        Group group = em.find(Group.class, groupId);
        if(!isMember(group, user))
        	throw new LoginException("User: " + user + " is not member of Group: " + groupId);
        
        EntityTransaction transaction = null;
        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();
            
            em.remove(group);
            
            transaction.commit();
            result = true;
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();

            log.error("Unhandled exception", ex);
            throw ex;
        } finally {
            // Close the EntityManager
            em.close();
        }
		
		log.info("Leave GroupDaoORMImpl.deleteGroup");
        return result;
	}

}
