package com.ymg.ymgapi.dao;

import com.ymg.ymgapi.dao.entity.Session;

public interface ISessionDao {

	public boolean addSession(Session session);
	
	public Session getSession(String user);
}
