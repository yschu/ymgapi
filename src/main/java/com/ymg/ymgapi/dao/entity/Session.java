package com.ymg.ymgapi.dao.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="session")
public class Session {

	@Id
	@Column(name="sessionToken", unique=true)
	private UUID sessionToken;
	
	@Column(name="username", nullable=false)
	private String user;
	
	@Column(name="createDateTime", nullable=false)
	private Date createDateTime;

	public UUID getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(UUID sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	} 
}
