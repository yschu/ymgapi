package com.ymg.ymgapi.dao.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="event")
public class Event {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", unique=true)
	private int id;
	
	@Column(name="owner", nullable=false)
	private String owner;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="description", nullable=true)
	private String description;
	
	@Column(name="url", nullable=true)
	private String url;
	
	@OneToMany(mappedBy="event", targetEntity=com.ymg.ymgapi.dao.entity.EventImage.class, cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private Set<com.ymg.ymgapi.dao.entity.EventImage> images;
	
	@OneToMany(mappedBy="event", targetEntity=com.ymg.ymgapi.dao.entity.EventSession.class, cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	private Set<com.ymg.ymgapi.dao.entity.EventSession> sessions;
	
	@OneToMany(mappedBy="event", targetEntity=com.ymg.ymgapi.dao.entity.EventTag.class, cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	private Set<com.ymg.ymgapi.dao.entity.EventTag> tags;
	
	@ManyToMany(mappedBy="events", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	private Collection<com.ymg.ymgapi.dao.entity.User> attendees;
	
	@ManyToMany
	@JoinTable(name = "group_event",
			joinColumns = {
					@JoinColumn(name="eventId", referencedColumnName="id")
			},
			inverseJoinColumns = {
					@JoinColumn(name="groupId", referencedColumnName="id")
			}
	)
	private Collection<Group> groups;
	
	@ManyToMany
	@JoinTable(name = "timeline_event",
			joinColumns = {
					@JoinColumn(name="eventId", referencedColumnName="id")
			},
			inverseJoinColumns = {
					@JoinColumn(name="timelineId", referencedColumnName="id")
			}
	)
	private Collection<Timeline> timelines;

	public Event(){
		this.images = new LinkedHashSet<com.ymg.ymgapi.dao.entity.EventImage>();
		this.sessions = new LinkedHashSet<com.ymg.ymgapi.dao.entity.EventSession>();
		this.setAttendees(new ArrayList<com.ymg.ymgapi.dao.entity.User>());
		this.setGroups(new ArrayList<com.ymg.ymgapi.dao.entity.Group>());
		this.setTimelines(new ArrayList<com.ymg.ymgapi.dao.entity.Timeline>());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<com.ymg.ymgapi.dao.entity.EventImage> getImages() {
		return images;
	}

	public void setImages(Set<com.ymg.ymgapi.dao.entity.EventImage> images) {
		this.images = images;
	}

	public Set<com.ymg.ymgapi.dao.entity.EventSession> getSessions() {
		return sessions;
	}

	public void setSessions(Set<com.ymg.ymgapi.dao.entity.EventSession> sessions) {
		this.sessions = sessions;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Collection<com.ymg.ymgapi.dao.entity.User> getAttendees() {
		return attendees;
	}

	public void setAttendees(Collection<com.ymg.ymgapi.dao.entity.User> attendees) {
		this.attendees = attendees;
	}

	public Collection<Group> getGroups() {
		return groups;
	}

	public void setGroups(Collection<Group> groups) {
		this.groups = groups;
	}

	public Collection<Timeline> getTimelines() {
		return timelines;
	}

	public void setTimelines(Collection<Timeline> timelines) {
		this.timelines = timelines;
	}

	public Set<com.ymg.ymgapi.dao.entity.EventTag> getTags() {
		return tags;
	}

	public void setTags(Set<com.ymg.ymgapi.dao.entity.EventTag> tags) {
		this.tags = tags;
	}
	
}
