package com.ymg.ymgapi.dao.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
@Table(name="timeline")
public class Timeline {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", unique=true)
	private int id;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="groupId", referencedColumnName="id")
	private Group group;
	
	@ManyToMany(mappedBy="timelines", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	private Collection<com.ymg.ymgapi.dao.entity.Event> events;
	
	public Timeline() {
		this.setEvents(new ArrayList<com.ymg.ymgapi.dao.entity.Event>());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Collection<com.ymg.ymgapi.dao.entity.Event> getEvents() {
		return events;
	}

	public void setEvents(Collection<com.ymg.ymgapi.dao.entity.Event> events) {
		this.events = events;
	}
}
