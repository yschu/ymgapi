package com.ymg.ymgapi.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="oAuthSession")
public class OAuthSession {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", unique=true)
	private int id;
	
	@Column(name="username", nullable=false)
	private String username;
	
	@Column(name="accessToken", nullable=false)
	private String accessToken;
	
	@Column(name="createDateTime", nullable=false)
	private Date createDateTime;
	
	@Column(name="expiryInMilliSec", nullable=false)
	private long expiryInMilliSec;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public long getExpiryInMilliSec() {
		return expiryInMilliSec;
	}

	public void setExpiryInMilliSec(long expiryInMilliSec) {
		this.expiryInMilliSec = expiryInMilliSec;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
