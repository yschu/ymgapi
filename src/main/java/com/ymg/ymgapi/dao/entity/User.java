package com.ymg.ymgapi.dao.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name= "userAccount")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", unique=true)
	private int id;
	
	@Column(name="username", nullable=false)
	private String username;
	
	@Column(name="password", nullable=false)
	private String password;
	
	@Column(name="displayName", nullable=false)
	private String displayName;
	
	@Column(name="email", nullable=true)
	private String email;
	
	@ManyToMany
	@JoinTable(name = "event_user",
		joinColumns = {
			@JoinColumn(name="userId", referencedColumnName="id")
		},
		inverseJoinColumns = {
			@JoinColumn(name="eventId", referencedColumnName="id")
		}
	)
	private Collection<Event> events;
	
	@ManyToMany
	@JoinTable(name="group_user",
			joinColumns= {
					@JoinColumn(name="userId", referencedColumnName="id")
			},
			inverseJoinColumns={
					@JoinColumn(name="groupId", referencedColumnName="id")
			}
	)
	private Collection<Group> groups;
	
	public User() {
		this.events = new ArrayList<Event>();
		this.setGroups(new ArrayList<com.ymg.ymgapi.dao.entity.Group>());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Collection<Event> getEvents() {
		return events;
	}

	public void setEvents(Collection<Event> events) {
		this.events = events;
	}

	public Collection<Group> getGroups() {
		return groups;
	}

	public void setGroups(Collection<Group> groups) {
		this.groups = groups;
	}
	
	
}
