package com.ymg.ymgapi.dao.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="group")
public class Group {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", unique=true)
	private int id;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@ManyToMany(mappedBy="groups", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	private Collection<com.ymg.ymgapi.dao.entity.Event> events;
	
	@ManyToMany(mappedBy="groups", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	private Collection<com.ymg.ymgapi.dao.entity.User> members;
	
	@OneToMany(mappedBy="group", targetEntity=com.ymg.ymgapi.dao.entity.Timeline.class, cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private Set<com.ymg.ymgapi.dao.entity.Timeline> timelines;
	
	public Group() {
		this.setEvents(new ArrayList<com.ymg.ymgapi.dao.entity.Event>());
		this.setMembers(new ArrayList<com.ymg.ymgapi.dao.entity.User>());
		this.setTimelines(new LinkedHashSet<com.ymg.ymgapi.dao.entity.Timeline>());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<com.ymg.ymgapi.dao.entity.Event> getEvents() {
		return events;
	}

	public void setEvents(Collection<com.ymg.ymgapi.dao.entity.Event> events) {
		this.events = events;
	}

	public Collection<com.ymg.ymgapi.dao.entity.User> getMembers() {
		return members;
	}

	public void setMembers(Collection<com.ymg.ymgapi.dao.entity.User> members) {
		this.members = members;
	}

	public Set<com.ymg.ymgapi.dao.entity.Timeline> getTimelines() {
		return timelines;
	}

	public void setTimelines(Set<com.ymg.ymgapi.dao.entity.Timeline> timelines) {
		this.timelines = timelines;
	}
}
