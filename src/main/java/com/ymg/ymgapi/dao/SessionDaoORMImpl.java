package com.ymg.ymgapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.entity.Session;

public class SessionDaoORMImpl implements ISessionDao {

	private static final Logger log = LoggerFactory.getLogger(SessionDaoORMImpl.class);
	
	private static EntityManagerFactory emf;
	
	public SessionDaoORMImpl(){
		emf = Persistence.createEntityManagerFactory("ymg-unit");
	}
	
	public boolean addSession(Session session) {
		log.info("Enter SessionDaoORMImpl.addSession");
		boolean result = false;
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction trx = null;
		
		try{
			trx = em.getTransaction();
			trx.begin();
			
			em.persist(session);
			
			trx.commit();
			result = true;
		} catch(Exception ex) {
			if(trx != null)
				trx.rollback();
			
			log.error("Unhandled exception", ex);
		} finally {
			em.close();
		}
		
		log.info("Leave SessionDaoORMImpl.addSession");
		return result;
	}

	public Session getSession(String user) {
		log.info("Enter SessionDaoORMImpl.getSession");
		Session session = null;

        // Create an EntityManager
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Get a List of Sessions
            List<Session> sessions = em.createQuery("SELECT s FROM Session s WHERE s.user = :user ORDER BY s.createDateTime DESC", Session.class)
            		.setParameter("user", user)
            		.setMaxResults(1)
            		.getResultList();
            
            if(sessions.size() > 0)
            	session = sessions.get(0);
            
            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null)
                transaction.rollback();
            
            log.error("Unhandled exception", ex);
        } finally {
            // Close the EntityManager
            em.close();
        }
        
        log.info("Leave SessionDaoORMImpl.getSession");
        return session;
	}

}
