package com.ymg.ymgapi.dao;

import java.util.Date;
import java.util.List;

import javax.security.auth.login.LoginException;

import com.ymg.ymgapi.dao.entity.Event;

public interface IEventDao {

	public List<Event> findAll(String user) throws Exception;
	
	public List<Event> findBetween(String user, Date startDateTime, Date endDateTime, String orderBy) throws Exception;
	
	public List<Event> findByLimit(String user, Date startDateTime, int limit, String orderBy) throws Exception;
	
	public int insertEvent(Event event) throws Exception;
	
	public boolean updateEvent(int eventId, String user, Event newEvent) throws LoginException, Exception;
	
	public boolean deleteEvent(int eventId, String user) throws LoginException, Exception;
}
