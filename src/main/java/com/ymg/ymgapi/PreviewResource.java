package com.ymg.ymgapi;

import java.io.IOException;
import java.util.Iterator;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dto.PreviewUrlReq;
import com.ymg.ymgapi.dto.PreviewUrlRsp;
import com.ymg.ymgapi.inspector.Authenticate;
import com.ymg.ymgapi.model.Event;
import com.ymg.ymgapi.model.EventImage;
import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.opengraph.ImgExtractor;
import com.ymg.ymgapi.opengraph.OpenGraph;

@Path("/v1/{user}/previews")
public class PreviewResource {

	private static final Logger log = LoggerFactory.getLogger(PreviewResource.class);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authenticate
	public PreviewUrlRsp previewUrl(@PathParam("user") String user, PreviewUrlReq req) {
		log.info("Enter PreviewResource.previewUrl(url={})", req.getUrl());
		PreviewUrlRsp rsp = new PreviewUrlRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			OpenGraph pageGraph = null;
			ImgExtractor imgExtractor = null;
			
			try {
				pageGraph = new OpenGraph(req.getUrl(), true);
				
				imgExtractor = new ImgExtractor(req.getUrl());
				imgExtractor.execute();
			} catch (IOException e) {
				log.error("Unhandled IOException", e);
			} catch (Exception e) {
				log.error("Unhandled Exception", e);
			}
			
			if(pageGraph != null) {
				Event event = new Event();
							
				event.setName(pageGraph.getContent("title"));
				event.setOwner(user);
				event.setDescription(pageGraph.getContent("description"));
				event.setUrl(req.getUrl());
				
				for(Iterator<String> imgIter = imgExtractor.getImageLinks().iterator(); imgIter.hasNext();)
				{
					String imgLink = imgIter.next();
					
					EventImage eventImage = new EventImage();
					eventImage.setUrl(imgLink);
					if(imgLink.equals(pageGraph.getContent("image")))
						eventImage.setDefault(true);
					else
						eventImage.setDefault(false);
					event.getImages().add(eventImage);
				}
				
				rsp.setEvent(event);
				
				rspStatus.setSuccess(true);
			} else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Fail to parse open graph");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(true);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave PreviewResource.previewUrl(eventName={})", rsp.getEvent().getName());
		return rsp;
	}
	
}
