package com.ymg.ymgapi;

import java.util.Date;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.IOAuthSessionDao;
import com.ymg.ymgapi.dao.OAuthSessionDaoORMImpl;
import com.ymg.ymgapi.dao.entity.OAuthSession;
import com.ymg.ymgapi.dto.GetAuthUrlRsp;
import com.ymg.ymgapi.dto.GetOAuthGraphRsp;
import com.ymg.ymgapi.dto.OAuthSignInReq;
import com.ymg.ymgapi.dto.OAuthSignInRsp;
import com.ymg.ymgapi.dto.RegisterReq;
import com.ymg.ymgapi.dto.RegisterRsp;
import com.ymg.ymgapi.dto.SignInReq;
import com.ymg.ymgapi.dto.SignInRsp;
import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.oauth.IOAuthHandler;
import com.ymg.ymgapi.oauth.OAuthAccessToken;
import com.ymg.ymgapi.oauth.OAuthGraph;
import com.ymg.ymgapi.oauth.OAuthHandlerFbImpl;
import com.ymg.ymgapi.util.Mapper;

@Path("v1/oauth")
public class OAuthResource {

	private static final Logger log = LoggerFactory.getLogger(OAuthResource.class);
	
	private static final String FACEBOOK_APP_ID = "288120284988657";
	private static final String FACEBOOK_APP_SECRET = "0f98bc3f5fd6ebc05abf5d262607da7c";
	private static final String REDIRECT_URL = "http://localhost:8080/fb/callback";
	
	private IOAuthHandler oAuthHandler;
	private CredentialResource credentialResource;
	private IOAuthSessionDao oAuthSessionDao;
	
	public OAuthResource() {
		this.oAuthHandler = new OAuthHandlerFbImpl(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET, REDIRECT_URL);
		this.credentialResource = new CredentialResource();
		this.oAuthSessionDao = new OAuthSessionDaoORMImpl();
	}
	
	@GET
	@Path("/url")
	@Produces(MediaType.APPLICATION_JSON)
	public GetAuthUrlRsp getAuthUrl(){
		log.info("Enter OAuthResource.getAuthUrl");
		GetAuthUrlRsp rsp = new GetAuthUrlRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			String url = this.oAuthHandler.getAuthUrl();
			rsp.setUrl(url);
			
			rspStatus.setSuccess(true);
		} catch(Exception ex) {
			log.error("Unhanled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave OAuthResource.getAuthUrl");
		return rsp;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public OAuthSignInRsp oAuthSignIn(OAuthSignInReq req){
		log.info("Enter OAuthResource.oAuthSignIn");
		OAuthSignInRsp rsp = new OAuthSignInRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			OAuthAccessToken accessToken = this.oAuthHandler.getAccessToken(req.getCode());
			OAuthGraph graph = this.oAuthHandler.getGraph(accessToken.getAccessToken());
			
			if(!this.credentialResource.getCredential(graph.getUserId()).isExist()) {
				RegisterReq registerReq = new RegisterReq();
				registerReq.setUsername(graph.getUserId());
				registerReq.setPassword(graph.getUserId());
				registerReq.setDisplayName(graph.getFirstName() + " " + graph.getLastName());
				registerReq.setEmail(graph.getEmail());
				
				RegisterRsp registerRsp = this.credentialResource.register(registerReq);
				
				if(!registerRsp.getRspStatus().isSuccess()) {
					log.error("Register new user fail");
					rsp.setAccessToken(UUID.fromString("00000000-0000-0000-0000-000000000000"));
					
					rspStatus.setSuccess(false);
					rspStatus.getErrors().add("Register new user fail");
					
					rsp.setRspStatus(rspStatus);
					
					return rsp;
				}
			}
			
			SignInReq signInReq = new SignInReq();
			signInReq.setUsername(graph.getUserId());
			signInReq.setPassword(graph.getUserId());
			SignInRsp signInRsp = this.credentialResource.signIn(signInReq);
			
			OAuthSession oAuthSession = new OAuthSession();
			oAuthSession.setUsername(graph.getUserId());
			oAuthSession.setAccessToken(accessToken.getAccessToken());
			oAuthSession.setCreateDateTime(new Date());
			oAuthSession.setExpiryInMilliSec(accessToken.getExpiryInMilliSec());
			this.oAuthSessionDao.addOAuthSession(oAuthSession);
		
			rsp.setAccessToken(signInRsp.getAccessToken());
			rsp.setUser(signInRsp.getUser());
			
			rspStatus.setSuccess(true);
			
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(true);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave OAuthResource.oAuthSignIn");
		return rsp;
	}
	
	@GET
	@Path("/graph")
	@Produces(MediaType.APPLICATION_JSON)
	public GetOAuthGraphRsp getOAuthGraph(@QueryParam("user") String username){
		log.info("Enter OAuthResource.getOAuthGraph");
		GetOAuthGraphRsp rsp = new GetOAuthGraphRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			OAuthSession oAuthSession = this.oAuthSessionDao.getOAuthSession(username);
			if(oAuthSession != null) {
				if((new Date()).getTime() < (oAuthSession.getCreateDateTime().getTime() + oAuthSession.getExpiryInMilliSec())) {
					com.ymg.ymgapi.oauth.OAuthGraph srcGraph = this.oAuthHandler.getGraph(oAuthSession.getAccessToken());
					com.ymg.ymgapi.model.OAuthGraph destGraph = Mapper.map(srcGraph);
					rsp.setGraph(destGraph);
					
					rspStatus.setSuccess(true);
				} else {
					rspStatus.setSuccess(false);
					rspStatus.getErrors().add("OAuth session expiried");
				}
			} else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("OAuth session does not exist");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave OAuthResource.getOAuthGraph");
		return rsp;
	}
}
