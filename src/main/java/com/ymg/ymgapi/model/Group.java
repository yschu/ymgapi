package com.ymg.ymgapi.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Group {

	private int id;
	private String name;
	private List<User> members;
	private List<Event> events;
	
	public Group() {
		this.setMembers(new ArrayList<User>());
		this.setEvents(new ArrayList<Event>());
	}

	public int getId() {
		return id;
	}

	@XmlElement
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public List<User> getMembers() {
		return members;
	}

	@XmlElement
	public void setMembers(List<User> members) {
		this.members = members;
	}

	public List<Event> getEvents() {
		return events;
	}

	@XmlElement
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	
}
