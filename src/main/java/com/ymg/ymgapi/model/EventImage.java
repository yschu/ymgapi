package com.ymg.ymgapi.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventImage {

	private String url;
	private boolean isDefault;
	
	public String getUrl() {
		return url;
	}
	
	@XmlElement
	public void setUrl(String url) {
		this.url = url;
	}
	
	public boolean isDefault() {
		return isDefault;
	}
	
	@XmlElement
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	
}
