package com.ymg.ymgapi.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponseStatus {

	private boolean isSuccess;
	private List<String> errors;
	
	public ResponseStatus() {
		this.isSuccess = false;
		this.errors = new ArrayList<String>();
	}
	
	public boolean isSuccess() {
		return isSuccess;
	}
	
	@XmlElement
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public List<String> getErrors() {
		return errors;
	}

	@XmlElement
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
