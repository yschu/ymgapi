package com.ymg.ymgapi.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventTag {

	private String tag;

	public String getTag() {
		return tag;
	}

	@XmlElement
	public void setTag(String tag) {
		this.tag = tag;
	}
}
