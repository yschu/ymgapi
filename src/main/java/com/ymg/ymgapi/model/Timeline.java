package com.ymg.ymgapi.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Timeline {

	private int id;
	private Group group;
	private List<Event> events;
	
	public Timeline() {
		this.setEvents(new ArrayList<Event>());
	}
	
	public int getId() {
		return id;
	}
	
	@XmlElement
	public void setId(int id) {
		this.id = id;
	}

	public Group getGroup() {
		return group;
	}

	@XmlElement
	public void setGroup(Group group) {
		this.group = group;
	}

	public List<Event> getEvents() {
		return events;
	}

	@XmlElement
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	
	
}
