package com.ymg.ymgapi.model;

import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Credential {

	private String user;
	private UUID accessToken;
	
	public String getUser() {
		return user;
	}
	
	@XmlElement
	public void setUser(String user) {
		this.user = user;
	}

	public UUID getAccessToken() {
		return accessToken;
	}

	@XmlElement
	public void setAccessToken(UUID accessToken) {
		this.accessToken = accessToken;
	}
	
}
