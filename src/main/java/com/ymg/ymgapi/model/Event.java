package com.ymg.ymgapi.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Event {

	private int id;
	private String owner;
	private String name;
	private String description;
	private String url;
	private List<EventImage> images;
	private List<EventSession> sessions;
	private List<EventTag> tags;
	private List<User> attendees;
	
	public Event() {
		this.images = new ArrayList<EventImage>();
		this.sessions = new ArrayList<EventSession>();
		this.setTags(new ArrayList<EventTag>());
		this.setAttendees(new ArrayList<User>());
	}
	
	public int getId() {
		return id;
	}
	
	@XmlElement
	public void setId(int id) {
		this.id = id;
	}
	
	public String getOwner() {
		return owner;
	}
	
	@XmlElement
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getName() {
		return name;
	}
	
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	@XmlElement
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<EventImage> getImages() {
		return images;
	}
	
	@XmlElement
	public void setImages(List<EventImage> images) {
		this.images = images;
	}
	
	
	public List<EventSession> getSessions() {
		return sessions;
	}
	
	@XmlElement
	public void setSessions(List<EventSession> sessions) {
		this.sessions = sessions;
	}

	public String getUrl() {
		return url;
	}

	@XmlElement
	public void setUrl(String url) {
		this.url = url;
	}

	public List<User> getAttendees() {
		return attendees;
	}

	@XmlElement
	public void setAttendees(List<User> attendees) {
		this.attendees = attendees;
	}

	public List<EventTag> getTags() {
		return tags;
	}

	@XmlElement
	public void setTags(List<EventTag> tags) {
		this.tags = tags;
	}
}
