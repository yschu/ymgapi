package com.ymg.ymgapi.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventSession {

	private Date from;
	private Date to;
	private String venue;
	private String description;
	
	public Date getFrom() {
		return from;
	}
	
	@XmlElement
	public void setFrom(Date from) {
		this.from = from;
	}
	
	public Date getTo() {
		return to;
	}
	
	@XmlElement
	public void setTo(Date to) {
		this.to = to;
	}
	
	public String getVenue() {
		return venue;
	}
	
	@XmlElement
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	public String getDescription() {
		return description;
	}
	
	@XmlElement
	public void setDescription(String description) {
		this.description = description;
	}
	
}
