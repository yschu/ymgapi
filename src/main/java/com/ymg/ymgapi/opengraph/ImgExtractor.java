package com.ymg.ymgapi.opengraph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ImgExtractor {

	private static final Logger log = LoggerFactory.getLogger(ImgExtractor.class);
	
	private String url;
	private List<String> imageLinks;
	
	public ImgExtractor(String url) {
		this.url = url;
		this.setImageLinks(new ArrayList<String>());
	}
	
	public void execute() throws IOException {
		log.info("Enter ImgExtractor.execute");
		
		// download the (X)HTML content, but only up to the closing head tag. We do not want to waste resources parsing irrelevant content
        URL pageURL = new URL(url);
        URLConnection siteConnection = pageURL.openConnection();
        BufferedReader dis = new BufferedReader(new InputStreamReader(siteConnection.getInputStream()));
        String inputLine;
        
        while ((inputLine = dis.readLine()) != null) {
        	int imgTagPos = inputLine.indexOf("<img");
        	if(imgTagPos > 0) {
        		int imgSrcPosStart = inputLine.indexOf("src", imgTagPos) + 5;
        		int imgSrcPosEnd = inputLine.indexOf("\"", imgSrcPosStart);
        		if(imgSrcPosStart > 0 && imgSrcPosEnd > imgSrcPosStart) {
        			String imgUrl = inputLine.substring(imgSrcPosStart, imgSrcPosEnd);
        			if(isValidImageUrl(imgUrl))
        				this.getImageLinks().add(imgUrl);
        		}
        	}
        }
        
		log.info("Leave ImgExtractor.execute");
	}
	
	private boolean isValidImageUrl(String url) {
		boolean result = false;
		
		if(url.startsWith("http") && 
		   (url.endsWith("jpg") || url.endsWith("png") || url.endsWith("gif")))
			result = true;
		
		return result;
	}
	
	public List<String> getImageLinks() {
		return imageLinks;
	}

	public void setImageLinks(List<String> imageLinks) {
		this.imageLinks = imageLinks;
	}
	
}
