package com.ymg.ymgapi.inspector;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Provider
@Authenticate
public class AuthenticateFilter implements ContainerRequestFilter {

	private static final Logger log = LoggerFactory.getLogger(AuthenticateFilter.class);

	public void filter(ContainerRequestContext arg) throws IOException {
		log.info("Enter AuthenticateFilter.filter");
		
		log.info("Leave AuthenticateFilter.filter");
	}


}
