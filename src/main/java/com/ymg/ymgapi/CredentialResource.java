package com.ymg.ymgapi;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ymg.ymgapi.dao.ISessionDao;
import com.ymg.ymgapi.dao.IUserDao;
import com.ymg.ymgapi.dao.SessionDaoORMImpl;
import com.ymg.ymgapi.dao.UserDaoORMImpl;
import com.ymg.ymgapi.dao.entity.Session;
import com.ymg.ymgapi.dao.entity.User;
import com.ymg.ymgapi.dto.GetCredentialRsp;
import com.ymg.ymgapi.dto.RegisterReq;
import com.ymg.ymgapi.dto.RegisterRsp;
import com.ymg.ymgapi.dto.SignInReq;
import com.ymg.ymgapi.dto.SignInRsp;
import com.ymg.ymgapi.model.ResponseStatus;
import com.ymg.ymgapi.util.Mapper;

@Path("v1/credentials")
public class CredentialResource {
	
	private static final Logger log = LoggerFactory.getLogger(CredentialResource.class);

	private IUserDao userDao;
	private ISessionDao sessionDao;
	
	public CredentialResource() {
		this.userDao = new UserDaoORMImpl();
		this.sessionDao = new SessionDaoORMImpl();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public GetCredentialRsp getCredential(@QueryParam("user") String user) {
		log.info("Enter CredentialResource.getCredential(user={})", user);
		GetCredentialRsp rsp = new GetCredentialRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try {
			rsp.setExist(false);
			
			List<User> users = this.userDao.getAllUsers();
			for(Iterator<User> userIter = users.iterator(); userIter.hasNext();)
			{
				User tmpUser = userIter.next();
				if(tmpUser.getUsername().equals(user))
				{
					rsp.setExist(true);
					break;
				}
			}
			
			rspStatus.setSuccess(true);
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave CredentialResource.getCredential(isExist={})", rsp.isExist());
		return rsp;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterRsp register(RegisterReq req) {
		log.info("Enter CredentialResource.register(username={})", req.getUsername());
		RegisterRsp rsp = new RegisterRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try{
			User user = new User();
			user.setUsername(req.getUsername());
			user.setPassword(req.getPassword());
			user.setEmail(req.getEmail());
			user.setDisplayName(req.getDisplayName());
			
			if(this.userDao.addUser(user))
				rspStatus.setSuccess(true);
			else {
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Database error: fail to add new user");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave CredentialResource.register(isSuccess={})", rsp.getRspStatus().isSuccess());
		return rsp;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SignInRsp signIn(SignInReq req) {
		log.info("Enter CredentialResource.signIn(username={})", req.getUsername());
		SignInRsp rsp = new SignInRsp();
		ResponseStatus rspStatus = new ResponseStatus();
		
		try{
			com.ymg.ymgapi.dao.entity.User srcUser = null;
			List<User> users = this.userDao.getAllUsers();
			for(Iterator<User> userIter = users.iterator(); userIter.hasNext();) {
				User tmpUser = userIter.next();
				if(tmpUser.getUsername().equals(req.getUsername()) && tmpUser.getPassword().equals(req.getPassword())) {
					srcUser = tmpUser;
					break;
				}
			}
			
			
			if(srcUser != null) {
				UUID sessionToken = UUID.randomUUID();
				
				Session session = new Session();
				session.setUser(req.getUsername());
				session.setSessionToken(sessionToken);
				session.setCreateDateTime(new Date());			
				this.sessionDao.addSession(session);
				
				rsp.setAccessToken(sessionToken);
				
				com.ymg.ymgapi.model.User destUser = Mapper.map(srcUser);
				rsp.setUser(destUser);
				
				rspStatus.setSuccess(true);
			} else {
				rsp.setAccessToken(UUID.fromString("00000000-0000-0000-0000-000000000000"));
				
				rspStatus.setSuccess(false);
				rspStatus.getErrors().add("Incorrect username or password");
			}
		} catch(Exception ex) {
			log.error("Unhandled exception", ex);
			rspStatus.setSuccess(false);
			rspStatus.getErrors().add(ex.getMessage());
		}
		
		rsp.setRspStatus(rspStatus);
		
		log.info("Leave CredentialResource.signIn(accessToken={})", rsp.getAccessToken());
		return rsp;
	}
}
